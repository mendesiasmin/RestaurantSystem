# Orientação a Objetos 2/2016
* Aluna: Iasmin Santos Mendes
* Matricula: 14/0041940

## EP2 - Java
Exercício Programa 2 da disciplina de Orientação a Objetos da Universidade de Brasília. O programa consiste em um sistema simples para gerenciamento de clientes e pedidos de um Restaurante fictício chamado "Tô com fome, quero mais".

### Execução

* Para executar é necessário ter uma máquina habilitada para rodar programas em java e uma IDE que facilite a execução.
* É recomendado o uso da IDE NetBeans 8.1, uma vez que todos os testes foram realizados neste ambiente.
* Ao inicializar o sistema, será apresentado a tela de login. As informações de acesso, previamente cadastradas, são respectivamente:
	Login: 000.000.000-00
	Senha: senha123
* De forma a facilitar os testes do programa foram cadastrados previamente alguns Clientes, Funcionários e Produtos, visando exemplificar melhor suas funcinalidades.
* O menu superior conta com o cadastro de clientes, funcionários e produtos	e as opções de fechar totalmente o programa ou fazer login com um funcionário diferente.
* O termo OS impresso na tela refere-se a Ordem de Serviço, o qual pode ser entendido como a comanda da mesa do restaurante associada.
* Sempre que executar uma ação sobre uma Ordem de Serviço ou um pedido, deve-se selecionar a linha referente nas tabelas apresentadas.
* As demais funcionalidades do programa são bem intuitivas e de fácil compreenssão.

