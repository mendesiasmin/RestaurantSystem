package model;

public class PayCard {

    private int cardType = 0;
    private int number_installments = 0;
    private float value = 0;

    /* cardType
    * 0 - Nenhum cartão
    * 1 - Debito
    * 2 - Crédito
    */
    public PayCard() {}
    
    public PayCard(int cardType, int number_installments, float value) {
        this.cardType = cardType;
        this.number_installments = number_installments;
        this.value = value;
    }
    
    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    public int getNumber_installments() {
        return number_installments;
    }

    public void setNumber_installments(int number_installments) {
        this.number_installments = number_installments;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
    
    public float getInstallmentsValue() {
        if(number_installments != 0) {
            return value/number_installments;
        }
        
        return 0;
    }
    
}
