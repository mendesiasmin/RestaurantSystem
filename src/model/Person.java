package model;


public abstract class Person {
	
	private String name;
	private String cpf;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) throws Exception {
	
            if(name != null && !name.isEmpty() && !name.trim().isEmpty()) {
                    this.name = name;
            }
            else {
                    throw new Exception("Nome Inválido.");
            }
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
            
            cpf = cpf.replaceAll(" ", "");
                      
            if(cpf != null && !cpf.isEmpty()  && !cpf.trim().isEmpty() && cpf.length() == 14) {
                    this.cpf = cpf;
            }
            else {
                throw new IllegalArgumentException("CPF Inválido.");
            }
	}
        
        public boolean authentic(String cpf) {
            return getCpf().equals(cpf);
        }
	
}
