/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author iasmin
 */

public class Order {
    
    private Stock product;
    private int amount;
    private String note;
    int status;

    public Order(Stock product, int amount, String note) throws Exception {
        setProduct(product);
        setAmount(amount);
        setNote(note);
        this.status = 1;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) throws Exception {
        if(this.status != -1){
            if(this.status != 0){
                this.status = status;
            } else {
                throw new Exception("Este pedido já foi entregue.");
            }
        } else {
            throw new Exception("Este pedido foi cancelado.");
        }
    }
    
    public void setProduct(Stock product) {
        this.product = product;
    }
    
    public Stock getProduct() {
        return this.product;
    }
    
    public void setAmount(int amount) throws Exception {
        
        if(amount > 0) {
            this.amount = amount;
        }
        else {
            throw new Exception("Quantidade Inválida.");
        }
    }
    
    public int getAmount() {
        return this.amount;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    
    public String getNote() {
        return this.note;
    }
    
    public Float orderPrice () {
        return getProduct().getProduct().getPrice() * amount;
    }
    
    public String toString() {
        return getProduct().getProduct().getName();
    }
    
    public float amountPay() {
        
        if(status == 0) {
            return amount * product.getProduct().getPrice();
        }
        
        return 0;
    }
    
}
