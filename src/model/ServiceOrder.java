/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ControllerOrder;

/**
 *
 * @author iasmin
 */
public class ServiceOrder {
    
    private Employee employee;
    private Client client;
    private ControllerOrder orderList;
    private String numberTable;
    private boolean open = true;

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public ControllerOrder getOrderList() {
        return orderList;
    }

    public void setOrderList(ControllerOrder orderList) {
        this.orderList = orderList;
    }
    
    public ServiceOrder(Employee employee, Client client, String numberTable) throws Exception {
        setEmployee(employee);
        setClient(client);
        setNumberTable(numberTable);
        this.orderList = new ControllerOrder();
    }

    public String getNumberTable() {
        return numberTable;
    }

    public void setNumberTable(String numberTable) throws Exception {
        if(numberTable != null && !numberTable.isEmpty() && !numberTable.trim().isEmpty()) {
            this.numberTable = numberTable;
        } else {
            throw new Exception("Mesa Inválida.");
        }
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    
    public float amountPay() {
    
        float payment = 0;
        for(Order o : orderList.getListOrder()) {
            payment += o.amountPay();
        }
        
        return payment;
    }
    
}
