package controller;

import model.ServiceOrder;

import java.util.ArrayList;


public class ControllerServiceOrder {
    
    private ArrayList<ServiceOrder> serviceOrderList;
   
    public ControllerServiceOrder() throws Exception {
        this.serviceOrderList = new ArrayList<>();
    }
    
    public ArrayList<ServiceOrder> getListOrder() throws Exception {
        return serviceOrderList;
    }
    
    public void add(ServiceOrder serviceOrder) throws Exception {
        
        if(serviceOrder != null){
            boolean flag = true;
            for(ServiceOrder o : this.serviceOrderList) {
                if(serviceOrder.getNumberTable().equals(o.getNumberTable()) && o.isOpen()){
                    flag = false;
                    break;
                }
            }
            if(flag){
                serviceOrderList.add(serviceOrder);
            } else {
                throw new Exception("Mesa Ocupada.");
            }
        } else {
            throw new Exception("Ordem de Serviço Inválida.");
        }
    }
    
    public void delete(ServiceOrder serviceOrder) {
        serviceOrderList.remove(serviceOrder);
    }
    
    public ServiceOrder getServiceOrder(String table) {
        for(ServiceOrder os : serviceOrderList) {
            if(os.getNumberTable().equals(table) && os.isOpen()){
                return os;
            }
        }
        
        return null;
    }
}
