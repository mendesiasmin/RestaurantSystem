package controller;

import model.Stock;

import java.util.ArrayList;
import model.Product;

public class ControllerStock {
    
    private ArrayList<Stock> stockList;
   
    public ControllerStock() throws Exception {
        this.stockList = new ArrayList<>();
        Product p = new Product("Salada", "10.5");
        Stock s = new Stock(p, 10, 100);
        this.stockList.add(s);
        p = new Product("Coca-cola 360ml", "3.5");
        s = new Stock(p, 10, 100);
        this.stockList.add(s);
        p = new Product("Doce de Leite", "2");
        s = new Stock(p, 10, 100);
        this.stockList.add(s);
    }
    
    public ArrayList<Stock> getListStock() {
        return stockList;
    }
    
    public void add(Stock stock) {
        stockList.add(stock);
    }
    
    public void delete(Stock stock) throws Exception {
        if(stock.getAmount() == 0){
            stockList.remove(stock);
        } else {
            throw new Exception("Ainda há itens deste produto em estoque.");
        }
    }
}
