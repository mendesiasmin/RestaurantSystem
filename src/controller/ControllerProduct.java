package controller;

import model.Product;

import java.util.ArrayList;

public class ControllerProduct {
    
    private ArrayList<Product> productList;
   
    public ControllerProduct() throws Exception {
        this.productList = new ArrayList<>();
    }
    
    public ArrayList<Product> getListProduct() {
        return productList;
    }
    
    public void add(Product product) {
        productList.add(product);
    }
    
    public void delete(Product product) {
        productList.remove(product);
    }
}
