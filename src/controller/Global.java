/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.HomeScreen;


/**
 *
 * @author iasmin
 */
public class Global {
    
    private static Global instance = null;
    
    /**
     *
     */
    public static ControllerEmployee employeeGlobal = null;
    public static ControllerClient clientGlobal = null;
    public static ControllerStock stockGlobal = null;
    public static ControllerServiceOrder serviceOrderGlobal = null;
    public static HomeScreen home = null;
    
    private Global() {}
    
    public static Global getInstance() throws Exception {
        if(instance == null){
            instance = new Global();
            employeeGlobal = new ControllerEmployee();
            clientGlobal = new ControllerClient();
            stockGlobal = new ControllerStock();
            serviceOrderGlobal = new ControllerServiceOrder();
        }
        return instance;
    }
}
