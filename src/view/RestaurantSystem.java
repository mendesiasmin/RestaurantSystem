package view;

import controller.Global;


public class RestaurantSystem {

    private Global instance;
    
    public RestaurantSystem() throws Exception {
        this.instance = Global.getInstance();
    }
    
    public static void main(String[] args) throws Exception {
              
        LoginScreen screen = new LoginScreen();
        screen.setVisible(true);
    }
 }
