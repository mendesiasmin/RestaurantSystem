/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Global;
import javax.swing.JOptionPane;
import model.Stock;

/**
 *
 * @author iasmin
 */
public class EditStock extends javax.swing.JFrame {

    private Stock product;
    /**
     * Creates new form EditStock
     */
    public EditStock(Stock product) {
        initComponents();
        this.product = product;
        productName.setText(product.getProduct().getName());
        price_field.setText(Float.toString(product.getProduct().getPrice()));
        amountStock.setValue(product.getAmount());
        minimumStock.setValue(product.getMininum());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        save_button = new javax.swing.JButton();
        chef_image = new javax.swing.JLabel();
        cancel_button = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        title_label = new javax.swing.JLabel();
        productName = new javax.swing.JTextField();
        price_field = new javax.swing.JTextField();
        name2 = new javax.swing.JLabel();
        name3 = new javax.swing.JLabel();
        amountStock = new javax.swing.JSpinner();
        minimumStock = new javax.swing.JSpinner();
        name1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menu1 = new javax.swing.JMenu();
        newEmployee = new javax.swing.JMenuItem();
        newClient = new javax.swing.JMenuItem();
        newProduct = new javax.swing.JMenuItem();
        out_option = new javax.swing.JMenu();
        chageUser_option = new javax.swing.JMenuItem();
        close_option = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        save_button.setText("Salvar");
        save_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_buttonActionPerformed(evt);
            }
        });

        chef_image.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/chef.png"))); // NOI18N

        cancel_button.setText("Cancelar");
        cancel_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancel_buttonActionPerformed(evt);
            }
        });

        jLabel3.setText("Produto");

        title_label.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        title_label.setText("Editar Produto");

        productName.setText("jTextField1");

        name2.setText("Quantidade Mínima em Estoque");

        name3.setText("Quantidade em Estoque");

        name1.setText("Preço");

        menu1.setText("Cadastrar");

        newEmployee.setText("Funcionário");
        newEmployee.setActionCommand("Novo Funcionário");
        newEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newEmployeeActionPerformed(evt);
            }
        });
        menu1.add(newEmployee);

        newClient.setText("Cliente");
        newClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newClientActionPerformed(evt);
            }
        });
        menu1.add(newClient);

        newProduct.setText("Produto");
        newProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newProductActionPerformed(evt);
            }
        });
        menu1.add(newProduct);

        jMenuBar1.add(menu1);

        out_option.setText("Sair");

        chageUser_option.setText("Trocar Usuário");
        chageUser_option.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chageUser_optionActionPerformed(evt);
            }
        });
        out_option.add(chageUser_option);

        close_option.setText("Fechar");
        close_option.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close_optionActionPerformed(evt);
            }
        });
        out_option.add(close_option);

        jMenuBar1.add(out_option);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(title_label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chef_image))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(50, 50, 50)
                        .addComponent(productName))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(name1)
                        .addGap(66, 66, 66)
                        .addComponent(price_field))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(name3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(amountStock))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(name2)
                        .addGap(18, 18, 18)
                        .addComponent(minimumStock, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cancel_button)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(save_button)
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(chef_image)
                    .addComponent(title_label))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(productName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(name1)
                    .addComponent(price_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(name3)
                    .addComponent(amountStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(name2)
                    .addComponent(minimumStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancel_button)
                    .addComponent(save_button))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void save_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_buttonActionPerformed

        try {
            if(JOptionPane.showConfirmDialog(rootPane, "Confirmar alterações?") == 0){
                this.product.getProduct().setName(productName.getText());
                this.product.getProduct().setPrice(price_field.getText());
                this.product.setAmount((int) amountStock.getValue());
                this.product.setMininum((int) minimumStock.getValue());
                Global.home.updateStockTable();
                dispose();
            }
        } catch (Exception error) {
            JOptionPane.showMessageDialog(rootPane, error.getMessage());
        }

    }//GEN-LAST:event_save_buttonActionPerformed

    private void cancel_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancel_buttonActionPerformed
        
        dispose();
    }//GEN-LAST:event_cancel_buttonActionPerformed

    private void newEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newEmployeeActionPerformed
        newEmployee screen;
        try {
            screen = new newEmployee();
            screen.setVisible(true);
        } catch (Exception ex) {

        }
        dispose();
    }//GEN-LAST:event_newEmployeeActionPerformed

    private void newClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newClientActionPerformed

        newClient screen;
        try {
            screen = new newClient();
            screen.setVisible(true);
        } catch (Exception ex) {
        }
    }//GEN-LAST:event_newClientActionPerformed

    private void newProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newProductActionPerformed
        newProduct screen = new newProduct();
        screen.setVisible(true);
        dispose();
    }//GEN-LAST:event_newProductActionPerformed

    private void chageUser_optionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chageUser_optionActionPerformed

        LoginScreen screen = new LoginScreen();
        screen.setVisible(true);
        dispose();
    }//GEN-LAST:event_chageUser_optionActionPerformed

    private void close_optionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_close_optionActionPerformed
        System.exit(0);
    }//GEN-LAST:event_close_optionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditStock.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditStock.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditStock.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditStock.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new EditStock().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSpinner amountStock;
    private javax.swing.JButton cancel_button;
    private javax.swing.JMenuItem chageUser_option;
    private javax.swing.JLabel chef_image;
    private javax.swing.JMenuItem close_option;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu menu1;
    private javax.swing.JSpinner minimumStock;
    private javax.swing.JLabel name1;
    private javax.swing.JLabel name2;
    private javax.swing.JLabel name3;
    private javax.swing.JMenuItem newClient;
    private javax.swing.JMenuItem newEmployee;
    private javax.swing.JMenuItem newProduct;
    private javax.swing.JMenu out_option;
    private javax.swing.JTextField price_field;
    private javax.swing.JTextField productName;
    private javax.swing.JButton save_button;
    private javax.swing.JLabel title_label;
    // End of variables declaration//GEN-END:variables
}
